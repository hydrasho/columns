namespace Texture {
	public sf.Texture load_from_resource(string path) throws Error
	{
		var data = resources_lookup_data(path, NONE);
		var texture = new sf.Texture.fromMemory(data.get_data(), data.length);
		if (texture == null)
			throw new ConvertError.FAILED("Failed to load texture from resource");
		return texture;
	}
}

namespace Font {
	public sf.Font load_from_resource(string path) throws Error
	{
		var data = resources_lookup_data(path, NONE);
		var font = new sf.Font.fromMemory(data.get_data(), data.length);
		if (font == null)
			throw new ConvertError.FAILED("Failed to load texture from resource");
		return font;
	}
}

