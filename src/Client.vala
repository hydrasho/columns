class Client {
	SocketClient socket; 
	SocketConnection conn; 

	public Client () {
		socket = new SocketClient ();
		conn = socket.connect (new InetSocketAddress.from_string ("127.0.0.1", 1024));

		read_message.begin();
	}

	async void read_message () {
		var input = new DataInputStream (conn.input_stream);

		while (true) {
			var msg = yield input.read_line_async ();
			if (msg == null) {
				onDeath ();
				break;
			}
			onReceive (msg);
		}
	}

	public signal void onDeath ();
	public signal void onReceive (string message);
}
