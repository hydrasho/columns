using sf;

private enum JewelType {
	NONE = 0,
	YELLOW = 1,
	ORANGE = 2,
	GREEN = 3,
	PURPLE = 4,
	RED = 5,
	BLUE = 6,
}

/**
 * Jewel class
 */
public class Jewel : Object {
	public sf.Sprite sprite;
	private static sf.Texture? texture = null;

	/**
	 * Constructor
	 */
	public Jewel() throws Error {
		if (texture == null)
			texture = Texture.load_from_resource("/data/image/jewel.bmp");
		this.sprite = new sf.Sprite();
		sprite.texture = Jewel.texture;
		jeweltype = JewelType.NONE;
	}

	/**
	 * Change the jewel type to a random type
	 */
	public void change_jewel_random () {
		var tmp = Random.int_range(1, 6);
		this.jeweltype = (JewelType)tmp;
	}

	/**
	 * Check if the jewel is colliding with another jewel
	 * @param jewels: Array of jewels
	 * @return bool: True if the jewel is colliding with another jewel
	 */
	private bool collide (Jewel []jewels) {
		if (this.sprite.position.y >= 768)
			return true;
		foreach (unowned var j in jewels) {
			if (j == this
				|| j.sprite.position.y == -128
				|| this.sprite.position.x != j.sprite.position.x
				|| this.sprite.position.y != j.sprite.position.y - 32
			)
				continue;
			return true;

		}
		return false;
	}

	private Timer timer_falling = new Timer();

	/**
	 * Make the jewel fall
	 * @param jewels: Array of jewels
	 * @param force: bool
	 */
	public void fall (Jewel []jewels, bool force = false) {
		if (timer_falling.elapsed() > 0.009) {
			if (collide(jewels) == false || force)
				this.sprite.move({0, 32});
			timer_falling.reset ();
		}
	}


	/**
	 * Draw the jewel
	 * @param window: RenderWindow
	 */
	public void draw (sf.RenderWindow window) {
		window.draw(this.sprite);
	}
	
	/**
	 * Set the position of the jewel
	 * @param position: Vector2f
	 */
	private JewelType jeweltype {
		get {
			return _type;
		}
		set {
			_type = value;
			sprite.textureRect = {(_type * 32)-32, 0, 32, 32};
		}
	}
	private JewelType _type = JewelType.NONE;
}


/**
 * Loading class
 */
public class Loading {
	public sf.Sprite sprite_sega;
	public sf.Texture texture[9];
	public Jewel jewels[128];

	/**
	 * Constructor
	 */
	public Loading() throws Error {
		this.texture[0] = Texture.load_from_resource("/data/sega.png");
		this.texture[1] = Texture.load_from_resource("/data/sega1.png");
		this.texture[2] = Texture.load_from_resource("/data/sega2.png");
		this.texture[3] = Texture.load_from_resource("/data/sega3.png");
		this.texture[4] = Texture.load_from_resource("/data/sega4.png");
		this.texture[5] = Texture.load_from_resource("/data/sega5.png");
		this.texture[6] = Texture.load_from_resource("/data/sega6.png");
		this.texture[7] = Texture.load_from_resource("/data/sega7.png");
		this.texture[8] = Texture.load_from_resource("/data/sega8.png");
		
		this.sprite_sega = new sf.Sprite() {
			texture = this.texture[0],
			origin = {this.texture[0].size.x / 2, this.texture[0].size.y / 2},
		};

		// Create the jewels
		for (int i = 0; i < jewels.length; ++i)
		{
			jewels[i] = new Jewel();
			jewels[i].sprite.position = {-128, -128};
			jewels[i].change_jewel_random();
		}
	}

	private int anime_it = 0;
	private Timer timer_anim = new Timer();
	private void get_next_anim (){
		if (timer_anim.elapsed() > 0.07) {
			if (is_falling == false && anime_it == 0)
				return ;
			++anime_it;
			if (anime_it == 9)
				anime_it = 0;
			this.sprite_sega.texture = this.texture[anime_it];
			timer_anim.reset();
			return ;
		}
	}
	/**
	 * Set the position of the loading
	 * @param position: Vector2f center position
	 */
	public void set_to_center (sf.Vector2f position) {
		this.sprite_sega.position = position;
	}

	private bool is_falling = true;
	private int n = 0;
	private Timer timer = new Timer();

	public void draw (sf.RenderWindow window) {

		window.draw(this.sprite_sega);
		get_next_anim();

		foreach (unowned var j in jewels) {
			j.draw(window);
		}

		// When end animation is done, start falling animation
		if (is_falling == false) {
			foreach (unowned var j in jewels) {
				j.fall(jewels, true);
			}
			uint8 a = this.sprite_sega.getColor ().a;
			if (a >= 0) {
				if (a <= 3) {
					next();
					a = 0;
				}
				else
					a -= 3;
				this.sprite_sega.setColor ({a,a,a,a});
			}
			return;
		}
		// if (is_falling == false) {
		// }

		// Begin animation falling jewels from top
		foreach (unowned var j in jewels) {
			if (j.sprite.position.y != -128)
				j.fall (jewels);
		}

		// Add jewels to the screen top
		if (timer.elapsed() > 0.05) {
			timer.reset();
			if (n < jewels.length) {
				for (int i = 0; i < 29; i++)
				{
					if (n >= jewels.length)
						break;
					int rand;
					if (0 < i < 7)
						rand = Random.int_range(0, 7);
					else if (8 < i < 19)
						rand = Random.int_range(0, 4);
					else
						rand = Random.int_range(0, 7);

					if (rand == 0) {
						jewels[n].sprite.position = {(i + 4) * 32, 0};
						n++;
					}
				}
			}
			else {
				// set falling animation to false after 500ms
				Timeout.add(500, () => {
					is_falling = false;
					return false;
				});
			}
		}
	}
	public async void loop (RenderWindow window) {
		bool stop = false;
		next.connect (()=> {
			stop = true;
		});
		while (window.isOpen() && stop == false)
		{
			Event event;

			while (window.pollEvent(out event))
			{
				if (event.type == EventType.Closed)
					window.close();
			}

			window.clear();
			this.draw(window);
			window.display();

			Idle.add(loop.callback);
			yield;
		}
	}
	public signal void next();
}

async void loading_app (RenderWindow window) throws Error {
	// Just Loading Sega logo
	Loading loading = new Loading();
	loading.set_to_center({window.size.x / 2, window.size.y / 2});
	yield loading.loop(window);
}

