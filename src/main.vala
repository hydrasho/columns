using sf;

public class Application {
	RenderWindow window;
	Client client;

	public Application () {
		window = new RenderWindow(VideoMode(1200, 800), "Supra Sega!");
		window.setFramerateLimit (120);
		client = new Client();
		client.onReceive.connect( (message) => {
			print("Received: %s\n", message);
		});
	}

	private void loop_event () {
		Event event;
		while (window.pollEvent(out event))
		{
			if (event.type == EventType.Closed)
				window.close();
		}
	}

	private void loop_draw () {

	}

	public async void run_main_app () throws Error{

		yield loading_app(window);

		while (window.isOpen())
		{
			loop_event();
			window.clear();
			loop_draw();
			window.display();

			Idle.add(run_main_app.callback);
			yield;
		}

	}
}

public async void main() {
	try {
		Application app = new Application();
		yield app.run_main_app();
	}
	catch (Error e) {
		print("Error: %s\n", e.message);
	}
}
